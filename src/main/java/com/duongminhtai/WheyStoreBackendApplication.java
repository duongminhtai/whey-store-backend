package com.duongminhtai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WheyStoreBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(WheyStoreBackendApplication.class, args);
//        BCryptPasswordEncoder bpt = new BCryptPasswordEncoder();
//        System.out.println(bpt.encode("123456"));
    }

}
