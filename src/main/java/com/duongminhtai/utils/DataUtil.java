package com.duongminhtai.utils;

import java.math.BigDecimal;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class DataUtil {
    public static boolean isEmptyString(String value) {
        return (null == value || value.trim().length() == 0);
    }

    /**
     * Checks if is empty list.
     *
     * @param value the value
     * @return true, if is empty list
     */
    @SuppressWarnings("rawtypes")
    public static boolean isEmptyList(List value) {
        return (null == value || value.isEmpty());
    }

    public static JsonObject getJsonObject(String strJson) throws Exception {
        JsonObject json = null;
        try {
            if (null != strJson && 0 != strJson.length()) {
                json = new Gson().fromJson(strJson, JsonObject.class);
            }
            if (json != null && isJsonObjectHasKey(json, "data")) {
                json = json.getAsJsonObject("data");
            }
        } catch (Exception ex) {
            json = null;
        }

        if (null == json) {
            throw new ApiValidateException(Constants.STATUS_BAD_REQUEST, null, "The JSON format is incorrect");
        }
        return json;
    }

    public static boolean isJsonObjectHasKey(JsonObject json, String key) {
        return json.has(key);
    }

    public static String getStringValueFromJsonByKey(JsonObject json, String key) {
        if (!json.has(key) || json.get(key).isJsonNull()) {
            return null;
        }
        return json.get(key).getAsString().trim();
    }

    public static String getStringValueFromJsonElementByKey(JsonElement jsonElement, String key) {
        JsonObject json = jsonElement.getAsJsonObject();
        return getStringValueFromJsonByKey(json, key);
    }

    public static Integer getIntValueFromJsonByKey(JsonObject json, String key) throws Exception {
        if (!json.has(key) || json.get(key).isJsonNull()) {
            return null;
        }
        try {
            return json.get(key).getAsInt();
        } catch (NumberFormatException e) {
            throw new ApiValidateException(Constants.STATUS_BAD_REQUEST, null, "The format json 'Integer' is incorrect");
        }
    }

    public static Double getDoubleValueFromJsonByKey(JsonObject json, String key) throws Exception {
        if (!json.has(key) || json.get(key).isJsonNull()) {
            return null;
        }
        try {
            return json.get(key).getAsDouble();
        } catch (NumberFormatException e) {
            throw new ApiValidateException(Constants.STATUS_BAD_REQUEST, null, "The format json 'Double' is incorrect");
        }
    }

    public static Float getFloatValueFromJsonByKey(JsonObject json, String key) throws Exception {
        if (!json.has(key) || json.get(key).isJsonNull()) {
            return null;
        }
        try {
            return json.get(key).getAsFloat();
        } catch (NumberFormatException e) {
            throw new ApiValidateException(Constants.STATUS_BAD_REQUEST, null, "The format json 'Float' is incorrect");
        }
    }

    public static BigDecimal getBigDecimalValueFromJsonByKey(JsonObject json, String key) throws Exception {
        if (!json.has(key) || json.get(key).isJsonNull()) {
            return null;
        }
        try {
            return json.get(key).getAsBigDecimal();
        } catch (Exception e) {
            throw new ApiValidateException(Constants.STATUS_BAD_REQUEST, null, "The format json 'BigDecimal' is incorrect");
        }
    }

    public static JsonArray getJsonArrayByKey(JsonObject json, String key) throws Exception {
        JsonArray jsons = null;
        try {
            if (json != null && isJsonObjectHasKey(json, key)) {
                jsons = json.getAsJsonArray(key);
                return jsons;
            }
        } catch (Exception e) {
            throw new ApiValidateException(Constants.STATUS_BAD_REQUEST, null, "The format json 'JsonArray' is incorrect");
        }
        return jsons;
    }

}
