
package com.duongminhtai.utils;

public class ConstantColumns {

    public static final String YYYY_MM_DD_FORMAT = "yyyy/MM/dd";
    public static final String CRUD = "crud";
    public static final String EX_KEY = "ex_key";
    public static final String DEL_FLG = "del_flg";
    public static final String COMPANY_USER_FLG = "company_user_flg";
    public static final String CREATE_DATE = "create_date";
    public static final String CREATE_BY = "create_by";
    public static final String CREATE_PG = "create_pg";
    public static final String UPDATE_DATE = "update_date";
    public static final String UPDATE_BY = "update_by";
    public static final String UPDATE_PG = "update_pg";
    public static final String UTIMESTAMP = "utimestamp";
    public static final String USER_ID_FORMAT = "user_id_format";

    public static final String DATETIME_SYS = "datetime_sys";

    // category
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_PARENT_ID = "category_parent_id";

    // product
    public static final String PRODUCT_ID = "product_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String PRODUCT_PRICE = "product_price";
    public static final String PRODUCT_QUANTITY = "product_quantity";
    public static final String PRODUCT_DESCRIPTION = "product_description";
    public static final String PRODUCT_DETAIL = "product_detail";
    public static final String PRODUCT_IMAGE = "product_image";
    public static final String PRODUCT_ORIGIN = "product_origin";
    public static final String PRODUCT_WEIGHT = "product_weight";
    public static final String PRODUCT_CARBOHYDRATE = "product_carbohydrate";
    public static final String PRODUCT_PROTEIN = "product_protein";
    public static final String PRODUCT_FAT = "product_fat";
    public static final String PRODUCT_CALORIES = "product_calories";
    public static final String PRODUCT_SERVINGS_PER_CONTAINER = "product_servings_per_container";
    public static final String PRODUCT_SERVING_SIZE = "product_serving_size";

    // order
    public static final String ORDER_ID = "order_id";
    public static final String TOTAL = "total";
    public static final String PAYMENT = "payment";
    public static final String STATUS = "status";
    public static final String TYPEPAYMENT = "type_payment";
    public static final String RECEIVER_NAME = "receiver_name";
    public static final String RECEIVER_PHONE = "receiver_phone";
    public static final String RECEIVER_ADDRESS = "receiver_address";

    // cart
    public static final String CART_ID = "cart_id";

    // order detail
    public static final String ORDER_DETAIL = "order_detail";
    public static final String ORDER_DETAIL_ID = "order_detail_id";
    public static final String PRODUCT_ORDER_QUANTITY = "product_order_quantity";
    public static final String PRODUCT_ORDER_PRICE = "product_order_price";

    // cart detail
    public static final String CART_DETAIL = "cart_detail";
    public static final String CART_DETAIL_ID = "cart_detail_id";
    public static final String PRODUCT_CART_QUANTITY = "product_cart_quantity";
    public static final String PRODUCT_CART_PRICE = "product_cart_price";

    // user
    public static final String USER_ID = "user_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String FULLNAME = "fullname";
    public static final String ROLE = "role";
    public static final String ACTIVE = "active";

}
