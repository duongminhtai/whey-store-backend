package com.duongminhtai.utils;

public class Constants {

    public static String DATE_PATTEN = "yyyy/MM/dd";

    // Status response
    public static final String STATUS_OK = "200";
    public static final String STATUS_201 = "201";
    public static final String STATUS_SYSTEM_ERROR = "500";
    public static final String STATUS_BAD_REQUEST = "400";
    public static final String STATUS_PERMISSION_DENIED = "403";

    // Message response
    public static final String PROPERTIES_ENTITY = "entity";
    public static final String PROPERTIES_MESSAGE = "message";
    public static final String PROPERTIES_RESULTS = "results";
    public static final String PROPERTIES_DATA = "data";
    public static final String PROPERTIES_META = "meta";
    public static final String PROPERTIES_CODE = "code";
    public static final String PROPERTIES_ITEM_NAME = "item_name";
    public static final String PROPERTIES_ITEM_MAIL = "mail_info";
    public static final String PROPERTIES_ITEM_CSV = "item_csv";
    public static final String PROPERTIES_APP = "application";
    public static final String ITEM_NULL = "TBD";
    public static String MAIL_HOST = "mail.host";
    public static String MAIL_PORT = "mail.port";
    public static String MAIL_USERNAME = "mail.username";
    public static String MAIL_PASSWORD = "mail.password";

    public static final String DEL_FLG_0 = "0";
    public static final String DEL_FLG_1 = "1";
}
