package com.duongminhtai.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.service.CartService;
import com.duongminhtai.service.CategoryService;
import com.duongminhtai.service.OrderService;
import com.duongminhtai.service.ProductService;
import com.duongminhtai.service.UserService;

@RestController
public class PublicController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    //user
    @RequestMapping(value = "/api/public/user", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> addUser(@RequestBody String json) throws Exception {
        ResultBean resultBean = userService.add(json);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    //product
    @RequestMapping(value = "/api/public/product/category/{category_id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getListProductByCategoryId(@PathVariable Integer category_id) throws Exception {
        ResultBean resultBean = productService.getListProductByCategoryId(category_id);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/public/product/{product_id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getProductById(@PathVariable Integer product_id) throws Exception {
        ResultBean resultBean = productService.getProductById(product_id);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/public/product", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getAllProduct() throws Exception {
        ResultBean resultBean = productService.getAllProduct();
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/public/product/search", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> searchProduct(@RequestParam String param, @RequestParam Integer categoryId, @RequestParam String weight,
            @RequestParam String origin) throws Exception {
        ResultBean resultBean = productService.searchProduct(param, categoryId, weight, origin);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    //category
    @RequestMapping(value = "/api/public/category", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getListCategory() throws Exception {
        ResultBean resultBean = categoryService.getListCategory();
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/public/category/{category_id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getCategoryById(@PathVariable Integer category_id) throws Exception {
        ResultBean resultBean = categoryService.getCategoryById(category_id);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    //cart
    @RequestMapping(value = "/api/public/cart", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getListOrder(HttpServletRequest request) throws Exception {
        ResultBean resultBean = cartService.getCart(request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/public/cart", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> addCart(@RequestBody String json, HttpServletRequest request) throws Exception {
        ResultBean resultBean = cartService.add(json, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/public/cart", method = RequestMethod.PUT, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> updateCart(@RequestBody String json, HttpServletRequest request) throws Exception {
        ResultBean resultBean = cartService.update(json, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/public/cart", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> deleteCart(HttpServletRequest request) throws Exception {
        ResultBean resultBean = cartService.delete(request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/public/cart/multil", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> addMultil(@RequestBody String json, HttpServletRequest request) throws Exception {
        ResultBean resultBean = cartService.addMultil(json, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/public/cart-detail/{cart_detail_id}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> deleteCartDetail(@PathVariable Integer cart_detail_id, HttpServletRequest request) throws Exception {
        ResultBean resultBean = cartService.deleteCartDetail(cart_detail_id, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    //order
    @RequestMapping(value = "/api/public/order/momo", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> momoPayment(@RequestBody String json, HttpServletRequest request) throws Exception {
        ResultBean resultBean = orderService.momoPayment(json, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/api/public/order/cod", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> codPayment(@RequestBody String json, HttpServletRequest request) throws Exception {
        ResultBean resultBean = orderService.codPayment(json, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/public/checkout/{order_id}", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> checkout(@PathVariable Integer order_id) throws Exception {
        ResultBean resultBean = orderService.checkout(order_id);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/public/order", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getAllOfCustomer(HttpServletRequest request) throws Exception {
        ResultBean resultBean = orderService.getAllOfCustomer(request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/api/public/order/cancel/{order_id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> cancelOrder(@PathVariable Integer order_id, HttpServletRequest request) throws Exception {
        ResultBean resultBean = orderService.cancelOrder(order_id, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

}
