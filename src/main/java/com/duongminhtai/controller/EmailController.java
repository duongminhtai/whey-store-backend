package com.duongminhtai.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.service.EmailService;

@RestController
@RequestMapping("/mail")

public class EmailController {

    private final Logger log = LoggerFactory.getLogger(EmailController.class);
    @Autowired
    private EmailService emailService;
    
    /**
     * send plain email with no body
     * @param json
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/send/plain", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> sendEmailPlain (@RequestBody String json) throws Exception {
        ResultBean resultBean = this.emailService.sendMailBody(json);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
        
    }
    
    
    @RequestMapping(value = "/send/aws", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> sendEmailAWS (@RequestBody String json) throws Exception {
        ResultBean resultBean = this.emailService.sendMailAWS(json);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
        
    }
}
