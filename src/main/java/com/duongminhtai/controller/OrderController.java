
package com.duongminhtai.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.service.OrderService;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/api/admin/order", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> getListOrder() throws Exception {
        ResultBean resultBean = orderService.getAllOrder();
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
    //
    //    @RequestMapping(value = "/api/admin/order/{order_id}", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
    //    public ResponseEntity<ResultBean> getOrderById(@PathVariable Integer order_id) throws Exception {
    //        ResultBean resultBean = orderService.getOrderById(order_id);
    //        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    //    }

    @RequestMapping(value = "/api/admin/order", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> add(@RequestBody String json, HttpServletRequest request) throws Exception {
        ResultBean resultBean = orderService.add(json, request);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/admin/check-out/{order_id}", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResultBean> checkOut(@PathVariable Integer order_id) throws Exception {
        ResultBean resultBean = orderService.checkout(order_id);
        return new ResponseEntity<ResultBean>(resultBean, HttpStatus.OK);
    }
}
