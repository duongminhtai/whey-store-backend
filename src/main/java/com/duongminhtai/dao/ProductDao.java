package com.duongminhtai.dao;

import java.util.List;

import com.duongminhtai.bean.jpa.ProductEntity;
import com.duongminhtai.bean.reponse.ProductReponse;

public interface ProductDao {

    public ProductEntity getProductById(Integer productId);

    public List<ProductReponse> getListProductByCategoryId(Integer categoryId);

    public List<ProductReponse> searchProduct(String param, Integer categoryId, String weight, String origin);

    public List<ProductEntity> getAllProduct();

    public void add(ProductEntity entity);

    public void update(ProductEntity entity);

    public boolean delete(Integer id);
}
