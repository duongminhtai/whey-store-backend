package com.duongminhtai.dao;

import java.util.List;

import com.duongminhtai.bean.jpa.CategoryEntity;

public interface CategoryDao {

    public List<CategoryEntity> getListCategory();

    public CategoryEntity getCategoryById(Integer id);

    public void add(CategoryEntity entity);

    public void update(CategoryEntity entity);

    public boolean delete(Integer id);
}
