package com.duongminhtai.dao;

import java.util.List;

import com.duongminhtai.bean.jpa.UserEntity;

public interface UserDao {

    public UserEntity getUserByUsername(String username);

    public UserEntity getUserById(Integer userId);

    public List<UserEntity> getAllUser();

    public void add(UserEntity entity);

    public void update(UserEntity entity);

    public boolean delete(Integer id);
}
