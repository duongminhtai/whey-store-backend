package com.duongminhtai.dao;

import java.util.List;

import com.duongminhtai.bean.jpa.OrderEntity;
import com.duongminhtai.bean.reponse.OrderReponse;

public interface OrderDao {

	public List<OrderReponse> getListOrder();

	public List<OrderReponse> getListOrderOfCustomer(Integer userId);

	public Integer getMaxIdOrder();

	public OrderEntity getOrderByIdMoMoCheck(Integer id);
	
	public OrderEntity getOrderById(Integer id);

	public void add(OrderEntity entity);

	public void update(OrderEntity entity);

	public boolean delete(Integer id);

}
