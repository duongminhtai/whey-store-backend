package com.duongminhtai.dao;

import java.util.List;

import com.duongminhtai.bean.jpa.OrderDetailEntity;

public interface OrderDetailDao {

    public List<OrderDetailEntity> getListOrderDetail();

    public OrderDetailEntity getOrderDetailByOrderId(Integer id);

    public void add(OrderDetailEntity entity);

    public void update(OrderDetailEntity entity);

    public boolean delete(Integer id);
}
