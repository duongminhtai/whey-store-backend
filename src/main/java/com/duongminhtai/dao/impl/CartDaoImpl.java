
package com.duongminhtai.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.duongminhtai.bean.jpa.CartEntity;
import com.duongminhtai.dao.CartDao;
import com.duongminhtai.utils.Constants;

@Repository
public class CartDaoImpl implements CartDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void add(CartEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(CartEntity entity) {
        entityManager.merge(entity);

    }

    @Override
    public boolean delete(Integer cartId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE CartEntity ");
        sql.append(" SET ");
        sql.append("    delFlg = :delFlg ");
        sql.append(" WHERE ");
        sql.append("    cartId = :cartId ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_1);
        query.setParameter("cartId", cartId);
        Integer result = query.executeUpdate();
        if (result.equals(0)) {
            return false;
        }
        return true;
    }

    @Override
    public int checkCartExist(Integer userId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT COUNT(*) ");
        sql.append(" FROM ");
        sql.append("    CartEntity c ");
        sql.append(" WHERE ");
        sql.append("    userId =:userId ");
        sql.append("    AND delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("userId", userId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        Long result = (Long) query.getSingleResult();
        int count = result.intValue();
        return count;
    }

    // @SuppressWarnings("unchecked")
    // @Override
    // public List<CartEntity> getListCart() {
    // StringBuilder sql = new StringBuilder();
    // sql.append(" SELECT new com.duongminhtai.bean.reponse.OrderReponse( ");
    // sql.append(" o.orderId, ");
    // sql.append(" c.customerId, ");
    // sql.append(" c.customerName, ");
    // sql.append(" o.status, ");
    // sql.append(" o.total, ");
    // sql.append(" o.receiverAddress, ");
    // sql.append(" o.receiverName, ");
    // sql.append(" o.receiverPhone, ");
    // sql.append(" o.createDate, ");
    // sql.append(" o.updateDate, ");
    // sql.append(" o.delFlg) ");
    // sql.append(" FROM ");
    // sql.append(" OrderEntity o ");
    // sql.append(" INNER JOIN ");
    // sql.append(" CustomerEntity c ");
    // sql.append(" ON o.customerId = c.customerId ");
    // sql.append(" WHERE ");
    // sql.append(" o.delFlg =:delFlg ");
    // Query query = entityManager.createQuery(sql.toString());
    // query.setParameter("delFlg", Constants.DEL_FLG_0);
    // List<CartEntity> listEntity = null;
    // try {
    // listEntity = query.getResultList();
    // } catch (NoResultException e) {
    // }
    // return listEntity;
    // }

    @Override
    public Integer getMaxIdCart() {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT MAX(cartId) ");
        sql.append(" FROM ");
        sql.append("    CartEntity c ");
        sql.append(" WHERE ");
        sql.append("    delFlg = :delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        Integer result = (Integer) query.getSingleResult();
        return result;
    }

    @Override
    public CartEntity getCartById(Integer cartId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    CartEntity c ");
        sql.append(" WHERE ");
        sql.append("    cartId =:cartId ");
        sql.append("    AND delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("cartId", cartId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        CartEntity entity = null;
        try {
            entity = (CartEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return entity;
    }

    @Override
    public CartEntity getCartByUserId(Integer userId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    CartEntity c ");
        sql.append(" WHERE ");
        sql.append("    userId =:userId ");
        sql.append("    AND delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("userId", userId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        CartEntity entity = null;
        try {
            entity = (CartEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return entity;
    }

}
