
package com.duongminhtai.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.duongminhtai.bean.jpa.ProductEntity;
import com.duongminhtai.bean.reponse.ProductReponse;
import com.duongminhtai.dao.ProductDao;
import com.duongminhtai.utils.Constants;
import com.duongminhtai.utils.DataUtil;

@Repository
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void add(ProductEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(ProductEntity entity) {
        entityManager.merge(entity);

    }

    @Override
    public boolean delete(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE ProductEntity ");
        sql.append(" SET ");
        sql.append("    delFlg = :delFlg ");
        sql.append(" WHERE ");
        sql.append("    id = :id ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_1);
        query.setParameter("id", id);
        Integer result = query.executeUpdate();
        if (result.equals(0)) {
            return false;
        }
        return true;
    }

    @Override
    public ProductEntity getProductById(Integer productId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    ProductEntity p ");
        sql.append(" WHERE ");
        sql.append("    p.productId =:productId ");
        sql.append("    AND p.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("productId", productId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        ProductEntity productEntity = null;
        try {
            productEntity = (ProductEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return productEntity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductReponse> getListProductByCategoryId(Integer categoryId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT new com.duongminhtai.bean.reponse.ProductReponse( ");
        sql.append("    p.productId, ");
        sql.append("    c.categoryId, ");
        sql.append("    c.categoryName, ");
        sql.append("    p.productName, ");
        sql.append("    p.productPrice, ");
        sql.append("    p.productQuantity, ");
        sql.append("    p.productDescription, ");
        sql.append("    p.productDetail, ");
        sql.append("    p.productImage, ");
        sql.append("    p.productOrigin, ");
        sql.append("    p.productWeight, ");
        sql.append("    p.productCarbohydrate, ");
        sql.append("    p.productProtein, ");
        sql.append("    p.productFat, ");
        sql.append("    p.productCalories, ");
        sql.append("    p.productServingSize, ");
        sql.append("    p.productServingsPerContainer, ");
        sql.append("    p.delFlg) ");
        sql.append(" FROM ");
        sql.append("    ProductEntity p ");
        sql.append("    INNER JOIN  ");
        sql.append("        CategoryEntity c ");
        sql.append("        ON c.categoryId = p.categoryId ");
        sql.append(" WHERE ");
        sql.append("    p.categoryId =:categoryId ");
        sql.append("    AND p.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("categoryId", categoryId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        List<ProductReponse> listProductReponse = null;
        try {
            listProductReponse = query.getResultList();
        } catch (NoResultException e) {
        }
        return listProductReponse;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductEntity> getAllProduct() {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    ProductEntity p ");
        sql.append(" WHERE ");
        sql.append("    p.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        List<ProductEntity> listProductEntity = null;
        try {
            listProductEntity = query.getResultList();
        } catch (NoResultException e) {
        }
        return listProductEntity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ProductReponse> searchProduct(String param, Integer categoryId, String weight, String origin) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT new com.duongminhtai.bean.reponse.ProductReponse( ");
        sql.append("    p.productId, ");
        sql.append("    c.categoryId, ");
        sql.append("    c.categoryName, ");
        sql.append("    p.productName, ");
        sql.append("    p.productPrice, ");
        sql.append("    p.productQuantity, ");
        sql.append("    p.productDescription, ");
        sql.append("    p.productDetail, ");
        sql.append("    p.productImage, ");
        sql.append("    p.productOrigin, ");
        sql.append("    p.productWeight, ");
        sql.append("    p.productCarbohydrate, ");
        sql.append("    p.productProtein, ");
        sql.append("    p.productFat, ");
        sql.append("    p.productCalories, ");
        sql.append("    p.productServingSize, ");
        sql.append("    p.productServingsPerContainer, ");
        sql.append("    p.delFlg) ");
        sql.append(" FROM ");
        sql.append("    ProductEntity p ");
        sql.append("    INNER JOIN  ");
        sql.append("        CategoryEntity c ");
        sql.append("        ON c.categoryId = p.categoryId ");
        sql.append(" WHERE 1=1 ");
        if (param != null && !DataUtil.isEmptyString(param)) {
            sql.append("    AND( p.productName LIKE :param ");
            sql.append("         OR p.productDescription LIKE :param ");
            sql.append("         OR p.productDetail LIKE :param) ");
        }
        if (categoryId != 0) {
            sql.append("    AND c.categoryId =:categoryId ");
        }
        if (weight != null && !DataUtil.isEmptyString(weight)) {
            sql.append("    AND p.productWeight =:weight ");
        }
        if (origin != null && !DataUtil.isEmptyString(origin)) {
            sql.append("    AND p.productOrigin =:origin ");
        }
        sql.append("    AND p.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        if (param != null && !DataUtil.isEmptyString(param)) {
            query.setParameter("param", "%" + param + "%");
        }
        if (categoryId != 0) {
            query.setParameter("categoryId", categoryId);
        }
        if (weight != null && !DataUtil.isEmptyString(weight)) {
            query.setParameter("weight", weight);
        }
        if (origin != null && !DataUtil.isEmptyString(origin)) {
            query.setParameter("origin", origin);
        }
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        List<ProductReponse> listProductReponse = null;
        try {
            listProductReponse = query.getResultList();
        } catch (NoResultException e) {
        }
        return listProductReponse;
    }

}
