
package com.duongminhtai.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.duongminhtai.bean.jpa.CartDetailEntity;
import com.duongminhtai.bean.reponse.CartDetailReponse;
import com.duongminhtai.dao.CartDetailDao;
import com.duongminhtai.utils.Constants;

@Repository
public class CartDetailDaoImpl implements CartDetailDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void add(CartDetailEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(CartDetailEntity entity) {
        entityManager.merge(entity);

    }

    @Override
    public boolean delete(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE CartDetailEntity ");
        sql.append(" SET ");
        sql.append("    delFlg = :delFlg ");
        sql.append(" WHERE ");
        sql.append("    id = :id ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_1);
        query.setParameter("id", id);
        Integer result = query.executeUpdate();
        if (result.equals(0)) {
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CartDetailReponse> getListCartDetailByCartId(Integer cartId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT new com.duongminhtai.bean.reponse.CartDetailReponse( ");
        sql.append(" cd.cartDetailId, ");
        sql.append(" p.productId, ");
        sql.append(" p.productPrice, ");
        sql.append(" p.productName, ");
        sql.append(" p.productImage, ");
        sql.append(" cd.productCartQuantity, ");
        sql.append(" cd.productCartPrice, ");
        sql.append(" cd.delFlg) ");
        sql.append(" FROM ");
        sql.append("    CartDetailEntity cd ");
        sql.append(" INNER JOIN  ");
        sql.append("    ProductEntity p ");
        sql.append("    ON cd.productId = p.productId ");
        sql.append(" WHERE ");
        sql.append("    cd.cartId =:cartId ");
        sql.append("    AND cd.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("cartId", cartId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        List<CartDetailReponse> ListReponse = null;
        try {
            ListReponse = query.getResultList();
        } catch (NoResultException e) {
        }
        return ListReponse;
    }

    @Override
    public int checkCartDetailExist(Integer cartDetailId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT COUNT(*) ");
        sql.append(" FROM ");
        sql.append("    CartDetailEntity cd ");
        sql.append(" WHERE ");
        sql.append("    cartDetailId =:cartDetailId ");
        sql.append("    AND delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("cartDetailId", cartDetailId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        Long result = (Long) query.getSingleResult();
        int count = result.intValue();
        return count;
    }

    @Override
    public CartDetailEntity getCartDetailEntityById(Integer cartDetailId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    CartDetailEntity cd ");
        sql.append(" WHERE ");
        sql.append("    cartDetailId =:cartDetailId ");
        sql.append("    AND delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("cartDetailId", cartDetailId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        CartDetailEntity entity = null;
        try {
            entity = (CartDetailEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return entity;
    }

}
