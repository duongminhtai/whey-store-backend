
package com.duongminhtai.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.duongminhtai.bean.jpa.OrderDetailEntity;
import com.duongminhtai.dao.OrderDetailDao;
import com.duongminhtai.utils.Constants;

@Repository
public class OrderDetailDaoImpl implements OrderDetailDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void add(OrderDetailEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(OrderDetailEntity entity) {
        entityManager.merge(entity);

    }

    @Override
    public boolean delete(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE OrderDetailEntity ");
        sql.append(" SET ");
        sql.append("    delFlg = :delFlg ");
        sql.append(" WHERE ");
        sql.append("    id = :id ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_1);
        query.setParameter("id", id);
        Integer result = query.executeUpdate();
        if (result.equals(0)) {
            return false;
        }
        return true;
    }

    @Override
    public OrderDetailEntity getOrderDetailByOrderId(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    OrderDetailEntity od ");
        sql.append(" WHERE ");
        sql.append("    id =:id ");
        sql.append("    AND delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("id", id);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        OrderDetailEntity entity = null;
        try {
            entity = (OrderDetailEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderDetailEntity> getListOrderDetail() {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    OrderEntity o ");
        sql.append(" WHERE ");
        sql.append("    delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        List<OrderDetailEntity> listEntity = null;
        try {
            listEntity = query.getResultList();
        } catch (NoResultException e) {
        }
        return listEntity;
    }

}
