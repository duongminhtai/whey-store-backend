
package com.duongminhtai.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.duongminhtai.bean.jpa.OrderEntity;
import com.duongminhtai.bean.reponse.OrderReponse;
import com.duongminhtai.dao.OrderDao;
import com.duongminhtai.utils.Constants;

@Repository
public class OrderDaoImpl implements OrderDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public void add(OrderEntity entity) {
		entityManager.persist(entity);
	}

	@Override
	public void update(OrderEntity entity) {
		entityManager.merge(entity);

	}

	@Override
	public boolean delete(Integer id) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE OrderEntity ");
		sql.append(" SET ");
		sql.append("    delFlg = :delFlg ");
		sql.append(" WHERE ");
		sql.append("    id = :id ");
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("delFlg", Constants.DEL_FLG_1);
		query.setParameter("id", id);
		Integer result = query.executeUpdate();
		if (result.equals(0)) {
			return false;
		}
		return true;
	}

	@Override
	public OrderEntity getOrderByIdMoMoCheck(Integer id) {
		StringBuilder sql = new StringBuilder();
		sql.append(" FROM ");
		sql.append("    OrderEntity o ");
		sql.append(" WHERE ");
		sql.append("    id =:id ");
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("id", id);
		OrderEntity entity = null;
		try {
			entity = (OrderEntity) query.getSingleResult();
		} catch (NoResultException e) {
		}
		return entity;
	}
	
	@Override
	public OrderEntity getOrderById(Integer id) {
		StringBuilder sql = new StringBuilder();
		sql.append(" FROM ");
		sql.append("    OrderEntity o ");
		sql.append(" WHERE ");
		sql.append("    id =:id ");
		sql.append("    AND o.delFlg =:delFlg ");
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("id", id);
		query.setParameter("delFlg", Constants.DEL_FLG_0);
		OrderEntity entity = null;
		try {
			entity = (OrderEntity) query.getSingleResult();
		} catch (NoResultException e) {
		}
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrderReponse> getListOrder() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT new com.duongminhtai.bean.reponse.OrderReponse( ");
		sql.append(" o.orderId, ");
		sql.append(" u.userId, ");
		sql.append(" u.fullname, ");
		sql.append(" o.status, ");
		sql.append(" o.payment, ");
		sql.append(" o.total, ");
		sql.append(" o.receiverAddress,  ");
		sql.append(" o.receiverName, ");
		sql.append(" o.receiverPhone, ");
		sql.append(" o.createDate, ");
		sql.append(" o.updateDate, ");
		sql.append(" o.delFlg) ");
		sql.append(" FROM ");
		sql.append("    OrderEntity o ");
		sql.append(" INNER JOIN  ");
		sql.append("    UserEntity u ");
		sql.append("    ON o.userId = u.userId ");
		sql.append(" WHERE ");
		sql.append("    o.delFlg =:delFlg ");
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("delFlg", Constants.DEL_FLG_0);
		List<OrderReponse> listReponse = null;
		try {
			listReponse = query.getResultList();
		} catch (NoResultException e) {
		}
		return listReponse;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrderReponse> getListOrderOfCustomer(Integer userId) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT new com.duongminhtai.bean.reponse.OrderReponse( ");
		sql.append(" o.orderId, ");
		sql.append(" u.userId, ");
		sql.append(" u.fullname, ");
		sql.append(" o.status, ");
		sql.append(" o.payment, ");
		sql.append(" o.total, ");
		sql.append(" o.receiverAddress,  ");
		sql.append(" o.receiverName, ");
		sql.append(" o.receiverPhone, ");
		sql.append(" o.createDate, ");
		sql.append(" o.updateDate, ");
		sql.append(" o.delFlg) ");
		sql.append(" FROM ");
		sql.append("    OrderEntity o ");
		sql.append(" INNER JOIN  ");
		sql.append("    UserEntity u ");
		sql.append("    ON o.userId = u.userId ");
		sql.append(" WHERE ");
		sql.append("    o.userId =:userId ");
		sql.append("    AND o.delFlg =:delFlg ");
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("userId", userId);
		query.setParameter("delFlg", Constants.DEL_FLG_0);
		List<OrderReponse> listReponse = null;
		try {
			listReponse = query.getResultList();
		} catch (NoResultException e) {
		}
		return listReponse;
	}

	@Override
	public Integer getMaxIdOrder() {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT MAX(orderId) ");
		sql.append(" FROM ");
		sql.append("    OrderEntity o ");
		sql.append(" WHERE ");
		sql.append("    delFlg = :delFlg ");
		Query query = entityManager.createQuery(sql.toString());
		query.setParameter("delFlg", Constants.DEL_FLG_0);
		Integer result = (Integer) query.getSingleResult();
		return result;
	}

}
