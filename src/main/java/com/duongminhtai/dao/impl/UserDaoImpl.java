
package com.duongminhtai.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.duongminhtai.bean.jpa.UserEntity;
import com.duongminhtai.dao.UserDao;
import com.duongminhtai.utils.Constants;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void add(UserEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(UserEntity entity) {
        entityManager.merge(entity);

    }

    @Override
    public boolean delete(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE UserEntity ");
        sql.append(" SET ");
        sql.append("    delFlg = :delFlg ");
        sql.append(" WHERE ");
        sql.append("    id = :id ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_1);
        query.setParameter("id", id);
        Integer result = query.executeUpdate();
        if (result.equals(0)) {
            return false;
        }
        return true;
    }

    @Override
    public UserEntity getUserByUsername(String username) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    UserEntity u ");
        sql.append(" WHERE ");
        sql.append("    u.username =:username ");
        sql.append("    AND u.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("username", username);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        UserEntity entity = null;
        try {
            entity = (UserEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<UserEntity> getAllUser() {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    UserEntity u ");
        sql.append(" WHERE ");
        sql.append("    u.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        List<UserEntity> listUserEntity = null;
        try {
            listUserEntity = query.getResultList();
        } catch (NoResultException e) {
        }
        return listUserEntity;
    }

    @Override
    public UserEntity getUserById(Integer userId) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    UserEntity u ");
        sql.append(" WHERE ");
        sql.append("    u.userId =:userId ");
        sql.append("    AND u.delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("userId", userId);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        UserEntity entity = null;
        try {
            entity = (UserEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return entity;
    }

}
