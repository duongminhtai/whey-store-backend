
package com.duongminhtai.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.duongminhtai.bean.jpa.CategoryEntity;
import com.duongminhtai.dao.CategoryDao;
import com.duongminhtai.utils.Constants;

@Repository
public class CategoryDaoImpl implements CategoryDao {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void add(CategoryEntity entity) {
        entityManager.persist(entity);
    }

    @Override
    public void update(CategoryEntity entity) {
        entityManager.merge(entity);

    }

    @Override
    public boolean delete(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append(" UPDATE CategoryEntity ");
        sql.append(" SET ");
        sql.append("    delFlg = :delFlg ");
        sql.append(" WHERE ");
        sql.append("    id = :id ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_1);
        query.setParameter("id", id);
        Integer result = query.executeUpdate();
        if (result.equals(0)) {
            return false;
        }
        return true;
    }

    @Override
    public CategoryEntity getCategoryById(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    CategoryEntity c ");
        sql.append(" WHERE ");
        sql.append("    id =:id ");
        sql.append("    AND delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("id", id);
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        CategoryEntity entity = null;
        try {
            entity = (CategoryEntity) query.getSingleResult();
        } catch (NoResultException e) {
        }
        return entity;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CategoryEntity> getListCategory() {
        StringBuilder sql = new StringBuilder();
        sql.append(" FROM ");
        sql.append("    CategoryEntity c ");
        sql.append(" WHERE ");
        sql.append("    delFlg =:delFlg ");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("delFlg", Constants.DEL_FLG_0);
        List<CategoryEntity> listEntity = null;
        try {
            listEntity = query.getResultList();
        } catch (NoResultException e) {
        }
        return listEntity;
    }

}
