package com.duongminhtai.dao;

import java.util.List;

import com.duongminhtai.bean.jpa.CartDetailEntity;
import com.duongminhtai.bean.reponse.CartDetailReponse;

public interface CartDetailDao {

    public List<CartDetailReponse> getListCartDetailByCartId(Integer cartId);

    public int checkCartDetailExist(Integer cartDetailId);

    public CartDetailEntity getCartDetailEntityById(Integer cartDetailId);

    public void add(CartDetailEntity entity);

    public void update(CartDetailEntity entity);

    public boolean delete(Integer id);
}
