package com.duongminhtai.dao;

import com.duongminhtai.bean.jpa.CartEntity;

public interface CartDao {

	public Integer getMaxIdCart();

	public CartEntity getCartById(Integer cartId);

	public CartEntity getCartByUserId(Integer userId);

	public int checkCartExist(Integer userId);

	public void add(CartEntity entity);

	public void update(CartEntity entity);

	public boolean delete(Integer cartId);
}
