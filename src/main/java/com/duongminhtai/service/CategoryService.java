package com.duongminhtai.service;

import com.duongminhtai.bean.ResultBean;

public interface CategoryService {
	public ResultBean getListCategory() throws Exception;

	public ResultBean getCategoryById(Integer id) throws Exception;

	public ResultBean add(String json) throws Exception;

	public ResultBean update(String json) throws Exception;

	public ResultBean delete(Integer categoryId) throws Exception;

}
