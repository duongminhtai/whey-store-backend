package com.duongminhtai.service;

import javax.servlet.http.HttpServletRequest;

import com.duongminhtai.bean.ResultBean;

public interface OrderService {

	public ResultBean getOrderById(Integer orderId) throws Exception;

	public ResultBean getAllOrder() throws Exception;

	public ResultBean cancelOrder(Integer orderId, HttpServletRequest request) throws Exception;

	public ResultBean getAllOfCustomer(HttpServletRequest request) throws Exception;

	public ResultBean add(String json, HttpServletRequest request) throws Exception;

	public ResultBean codPayment(String json, HttpServletRequest request) throws Exception;

	public ResultBean momoPayment(String json, HttpServletRequest request) throws Exception;

	public ResultBean update(String json) throws Exception;

	public ResultBean checkout(Integer orderId) throws Exception;

	public ResultBean delete(String json) throws Exception;
}
