package com.duongminhtai.service;

import javax.servlet.http.HttpServletRequest;

import com.duongminhtai.bean.ResultBean;

public interface CartService {

	public ResultBean getCart(HttpServletRequest request) throws Exception;

	public ResultBean add(String json, HttpServletRequest request) throws Exception;

	public ResultBean addMultil(String json, HttpServletRequest request) throws Exception;

	public ResultBean update(String json, HttpServletRequest request) throws Exception;

	public ResultBean delete(HttpServletRequest request) throws Exception;

	public ResultBean deleteCartDetail(Integer cartDetailId, HttpServletRequest request) throws Exception;

}
