package com.duongminhtai.service;

import com.duongminhtai.bean.ResultBean;

public interface ProductService {

    public ResultBean getProductById(Integer productId) throws Exception;

    public ResultBean getListProductByCategoryId(Integer categoryId) throws Exception;

    public ResultBean getAllProduct() throws Exception;

    public ResultBean searchProduct(String param, Integer categoryId, String weight, String origin) throws Exception;

    public ResultBean add(String json) throws Exception;

    public ResultBean update(String json) throws Exception;

    public ResultBean delete(String json) throws Exception;

}
