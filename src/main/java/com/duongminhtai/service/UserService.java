package com.duongminhtai.service;

import com.duongminhtai.bean.ResultBean;

public interface UserService {

    public ResultBean getUserById(Integer userId) throws Exception;

    public ResultBean getAllUser() throws Exception;

    public ResultBean add(String json) throws Exception;

    public ResultBean update(String json) throws Exception;

    public ResultBean delete(String json) throws Exception;

}
