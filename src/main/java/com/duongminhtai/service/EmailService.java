package com.duongminhtai.service;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;

import com.duongminhtai.bean.ResultBean;

public interface EmailService {

    public ResultBean sendMailBody(String json) throws Exception;

    public ResultBean sendMailAWS(String json) throws UnsupportedEncodingException, MessagingException;

}
