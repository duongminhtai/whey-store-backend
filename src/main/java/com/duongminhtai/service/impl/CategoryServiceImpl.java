package com.duongminhtai.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.bean.jpa.CategoryEntity;
import com.duongminhtai.dao.CategoryDao;
import com.duongminhtai.service.CategoryService;
import com.duongminhtai.utils.ApiValidateException;
import com.duongminhtai.utils.ConstantColumns;
import com.duongminhtai.utils.Constants;
import com.duongminhtai.utils.DataUtil;
import com.google.gson.JsonObject;

@Service
@Transactional(rollbackOn = { ApiValidateException.class, Exception.class })
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @Override
    public ResultBean add(String json) throws Exception {
        CategoryEntity entity = new CategoryEntity();
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        this.getCategoryEntity(jsonObject, entity);
        categoryDao.add(entity);
        return new ResultBean(entity, Constants.STATUS_201, "Add successfully!!!");
    }

    @Override
    public ResultBean update(String json) throws Exception {
        CategoryEntity entity = new CategoryEntity();
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        this.getCategoryEntity(jsonObject, entity);
        categoryDao.update(entity);
        return new ResultBean(entity, Constants.STATUS_201, "Update successfully!!!");
    }

    @Override
    public ResultBean delete(Integer categoryId) throws Exception {
        this.categoryDao.delete(categoryId);
        return new ResultBean("200", "Delete category successfully!!!");
    }

    @Override
    public ResultBean getCategoryById(Integer id) throws Exception {
        CategoryEntity entity = categoryDao.getCategoryById(id);
        return new ResultBean(entity, Constants.STATUS_OK, "Get category by id successfully!!!");
    }

    @Override
    public ResultBean getListCategory() throws Exception {
        List<CategoryEntity> listEntity = categoryDao.getListCategory();
        return new ResultBean(listEntity, Constants.STATUS_OK, "Get list category successfully!!!");
    }

    private void getCategoryEntity(JsonObject json, CategoryEntity entity) throws Exception {
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.CATEGORY_ID)) {
            entity.setCategoryId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.CATEGORY_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.CATEGORY_NAME)) {
            entity.setCategoryName(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.CATEGORY_NAME));
        }
    }

}
