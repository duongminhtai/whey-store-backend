package com.duongminhtai.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.service.EmailService;
import com.duongminhtai.utils.ApiValidateException;
import com.duongminhtai.utils.Constants;
import com.duongminhtai.utils.DataUtil;
import com.google.gson.JsonObject;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional(rollbackOn = { ApiValidateException.class, Exception.class })
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    private final Logger log = LoggerFactory.getLogger(EmailService.class);

    /**
     * sendMailBody
     * 
     * @throws Exception
     */
    @Override
    public ResultBean sendMailBody(String json) throws Exception {
        // refer this: https://stackjava.com/demo/gui-gmail-bang-java.html
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        //
        String emailFrom = DataUtil.getStringValueFromJsonByKey(jsonObject, "email_from");
        String emailTo = DataUtil.getStringValueFromJsonByKey(jsonObject, "email_to");
        String emailTitle = DataUtil.getStringValueFromJsonByKey(jsonObject, "email_title");
        String emailContent = DataUtil.getStringValueFromJsonByKey(jsonObject, "email_content");

        String myMail = "scrum-app@bappartners.com";
        String myMailPassword = "bc@145248";

        Properties mailServerProperties;
        Session getMailSession;
        MimeMessage mailMessage;

        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");

        getMailSession = Session.getDefaultInstance(mailServerProperties, null);
        mailMessage = new MimeMessage(getMailSession);

        mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
        mailMessage.setSubject(emailTitle);
        mailMessage.setText(emailContent);
        String emailBody = "<p style='color: red'>Demo send HTML from Java<p>";
        mailMessage.setContent(emailBody, "text/html");

        Transport transport = getMailSession.getTransport("smtp");

        transport.connect("email-smtp.us-west-2.amazonaws.com", myMail, myMailPassword);
        transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
        transport.close();
        return new ResultBean(Constants.STATUS_OK, "mail sent!");
    }

    /**
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     * 
     */
    @Override
    public ResultBean sendMailAWS(String json) throws UnsupportedEncodingException, MessagingException {

        // refer to this: https://docs.aws.amazon.com/ses/latest/DeveloperGuide/examples-send-using-smtp.html
        // Replace sender@example.com with your "From" address.
        // This address must be verified.
        final String FROM = "scrum-app@bappartners.com";
        final String FROMNAME = "Scrum-App";

        // Replace recipient@example.com with a "To" address. If your account
        // is still in the sandbox, this address must be verified.
        final String TO = "sytv@bap.jp";

        // Replace smtp_username with your Amazon SES SMTP user name.
        final String SMTP_USERNAME = "AKIASLMEICXY325IU4E4";

        // Replace smtp_password with your Amazon SES SMTP password.
        final String SMTP_PASSWORD = "BPXPSnNTie+CimL9EWTCeLxZgmWvc7yWm9VkBY7jxItf";

        // The name of the Configuration Set to use for this message.
        // If you comment out or remove this variable, you will also need to
        // comment out or remove the header below.
        // final String CONFIGSET = "ConfigSet";

        // Amazon SES SMTP host name. This example uses the US West (Oregon) region.
        // See
        // https://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html#region-endpoints
        // for more information.
        final String HOST = "email-smtp.us-west-2.amazonaws.com";

        // The port you will connect to on the Amazon SES SMTP endpoint.
        final int PORT = 587;

        final String SUBJECT = "Amazon SES test (SMTP interface accessed using Java)";

        final String BODY = String.join(System.getProperty("line.separator"), "<h1>Amazon SES SMTP Email Test</h1>",
                "<p>This email was sent with Amazon SES using the ",
                "<a href='https://github.com/javaee/javamail'>Javamail Package</a>",
                " for <a href='https://www.java.com'>Java</a>.");

        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified
        // properties.
        Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information.
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM, FROMNAME));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
        msg.setSubject(SUBJECT);
        msg.setContent(BODY, "text/html");

        // Add a configuration set header. Comment or delete the
        // next line if you are not using a configuration set
        // msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);

        // Create a transport.
        Transport transport = session.getTransport();

        // Send the message.
        try {
            System.out.println("Sending...");

            // Connect to Amazon SES using the SMTP username and password you specified
            // above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        } catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        } finally {
            // Close and terminate the connection.
            transport.close();
        }
        return new ResultBean(Constants.STATUS_OK, "mail sent!");
    }

}
