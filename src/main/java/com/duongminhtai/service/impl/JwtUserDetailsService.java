package com.duongminhtai.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.duongminhtai.bean.jpa.UserEntity;
import com.duongminhtai.dao.UserDao;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity entity = userDao.getUserByUsername(username);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>(); // use list if you wish
        grantedAuthorities.add(new SimpleGrantedAuthority(entity.getRole()));
        return new org.springframework.security.core.userdetails.User(entity.getUsername(), entity.getPassword(),
                grantedAuthorities);
    }
}
