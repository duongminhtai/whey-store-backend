package com.duongminhtai.service.impl;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.bean.jpa.CartEntity;
import com.duongminhtai.bean.jpa.OrderDetailEntity;
import com.duongminhtai.bean.jpa.OrderEntity;
import com.duongminhtai.bean.jpa.ProductEntity;
import com.duongminhtai.bean.jpa.UserEntity;
import com.duongminhtai.bean.reponse.CartDetailReponse;
import com.duongminhtai.bean.reponse.OrderReponse;
import com.duongminhtai.config.JwtTokenUtil;
import com.duongminhtai.dao.CartDao;
import com.duongminhtai.dao.CartDetailDao;
import com.duongminhtai.dao.OrderDao;
import com.duongminhtai.dao.OrderDetailDao;
import com.duongminhtai.dao.ProductDao;
import com.duongminhtai.dao.UserDao;
import com.duongminhtai.service.OrderService;
import com.duongminhtai.utils.ApiValidateException;
import com.duongminhtai.utils.ConstantColumns;
import com.duongminhtai.utils.Constants;
import com.duongminhtai.utils.DataUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mservice.allinone.models.CaptureMoMoResponse;
import com.mservice.allinone.processor.allinone.CaptureMoMo;
import com.mservice.shared.sharedmodels.Environment;

@Service
@Transactional(rollbackOn = { ApiValidateException.class, Exception.class })
public class OrderServiceImpl implements OrderService {

    @Autowired
    private JwtTokenUtil jwtUtil;

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private OrderDetailDao orderDetailDao;

    @Autowired
    private CartDao cartDao;

    @Autowired
    private CartDetailDao cartDetailDao;

    @Autowired
    private ProductDao productDao;

    @Override
    public ResultBean add(String json, HttpServletRequest request) throws Exception {
        // Get user from token
        // Get Header, that's why we send HttpServletRequest
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
        UserEntity userEntity = userDao.getUserByUsername(username);
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        // add order
        OrderEntity orderEntity = new OrderEntity();
        this.getOrderEntity(jsonObject, orderEntity);
        orderEntity.setUserId(userEntity.getUserId());
        orderDao.add(orderEntity);
        // add order detail
        Integer orderId = orderDao.getMaxIdOrder();
        JsonArray jsonArray = new JsonArray();
        if (DataUtil.isJsonObjectHasKey(jsonObject, ConstantColumns.ORDER_DETAIL)) {
            jsonArray = DataUtil.getJsonArrayByKey(jsonObject, ConstantColumns.ORDER_DETAIL);
            for (JsonElement jsonElement : jsonArray) {
                OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
                ProductEntity productEntity = new ProductEntity();
                this.getOrderDetailEntity((JsonObject) jsonElement, orderDetailEntity);
                orderDetailEntity.setOrderId(orderId);
                orderDetailDao.add(orderDetailEntity);
                productEntity = productDao.getProductById(orderDetailEntity.getProductId());
                productEntity.setProductQuantity(
                        productEntity.getProductQuantity() - orderDetailEntity.getProductOrderQuantity());
                productDao.update(productEntity);
            }
        }

        if (DataUtil.isJsonObjectHasKey(jsonObject, ConstantColumns.CART_ID)) {
            Integer cartId = DataUtil.getIntValueFromJsonByKey(jsonObject, ConstantColumns.CART_ID);
            cartDao.delete(cartId);
        }

        return new ResultBean(orderEntity, Constants.STATUS_201, "Add successfully!!!");
    }

    @Override
    public ResultBean codPayment(String json, HttpServletRequest request) throws Exception {
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
        UserEntity userEntity = userDao.getUserByUsername(username);

        JsonObject jsonObject = DataUtil.getJsonObject(json);
        // get cart
        CartEntity cartEntity = cartDao.getCartByUserId(userEntity.getUserId());

        // add order
        OrderEntity orderEntity = new OrderEntity();
        this.getOrderEntity(jsonObject, orderEntity);
        orderEntity.setTotal(cartEntity.getTotal());
        orderEntity.setUserId(cartEntity.getUserId());
        orderEntity.setStatus("Chưa xác nhận");
        orderDao.add(orderEntity);

        // add order detail
        Integer orderId = orderDao.getMaxIdOrder();
        List<CartDetailReponse> listCartDetailReponse = cartDetailDao.getListCartDetailByCartId(cartEntity.getCartId());
        for (CartDetailReponse cartDetailReponse : listCartDetailReponse) {
            ProductEntity productEntity = productDao.getProductById(cartDetailReponse.getProductId());
            productEntity.setProductQuantity(
                    productEntity.getProductQuantity() - cartDetailReponse.getProductCartQuantity());
            productDao.update(productEntity);
            OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
            orderDetailEntity.setOrderId(orderId);
            orderDetailEntity.setProductId(cartDetailReponse.getProductId());
            orderDetailEntity.setProductOrderPrice(cartDetailReponse.getProductCartPrice());
            orderDetailEntity.setProductOrderQuantity(cartDetailReponse.getProductCartQuantity());
            orderDetailDao.add(orderDetailEntity);
        }

        // delete cart
        cartDao.delete(cartEntity.getCartId());
        return new ResultBean(orderEntity, Constants.STATUS_201, "COD Payment successfully!!!");
    }

    @Override
    public ResultBean momoPayment(String json, HttpServletRequest request) throws Exception {
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
        UserEntity userEntity = userDao.getUserByUsername(username);

        JsonObject jsonObject = DataUtil.getJsonObject(json);
        // get cart
        CartEntity cartEntity = cartDao.getCartByUserId(userEntity.getUserId());

        // add order
        OrderEntity orderEntity = new OrderEntity();
        this.getOrderEntity(jsonObject, orderEntity);
        orderEntity.setTotal(cartEntity.getTotal());
        orderEntity.setUserId(cartEntity.getUserId());
        orderEntity.setStatus("Chưa xác nhận");
        orderEntity.setDelFlg("1");
        orderDao.add(orderEntity);

        // add order detail
        Integer orderId = orderDao.getMaxIdOrder();
        List<CartDetailReponse> listCartDetailReponse = cartDetailDao.getListCartDetailByCartId(cartEntity.getCartId());
        for (CartDetailReponse cartDetailReponse : listCartDetailReponse) {
            ProductEntity productEntity = productDao.getProductById(cartDetailReponse.getProductId());
            productEntity.setProductQuantity(
                    productEntity.getProductQuantity() - cartDetailReponse.getProductCartQuantity());
            productDao.update(productEntity);
            OrderDetailEntity orderDetailEntity = new OrderDetailEntity();
            orderDetailEntity.setOrderId(orderId);
            orderDetailEntity.setProductId(cartDetailReponse.getProductId());
            orderDetailEntity.setProductOrderPrice(cartDetailReponse.getProductCartPrice());
            orderDetailEntity.setProductOrderQuantity(cartDetailReponse.getProductCartQuantity());
            orderDetailDao.add(orderDetailEntity);
        }

        // delete cart
        cartDao.delete(cartEntity.getCartId());

        // momo payment
        CaptureMoMoResponse captureMoMoResponse = null;
        String requestId = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String orderIdMoMo = String.valueOf(Calendar.getInstance().getTimeInMillis());

        // trả về trình duyệt
        String returnURL = "https://deploy-public.web.app/success";
        // trả về backend
        String notifyURL = "https://whey-store.herokuapp.com/api/public/checkout/" + orderEntity.getOrderId();
        String extraData = "merchantName=Whey Store";
        Environment environment = Environment.selectEnv(Environment.EnvTarget.DEV, Environment.ProcessType.PAY_GATE);

        // Tao yeu cau
        captureMoMoResponse = CaptureMoMo.process(environment, orderIdMoMo, requestId,
                Long.toString(cartEntity.getTotal().longValue()), "Thanh toán đơn hàng Whey store", returnURL,
                notifyURL, extraData);

        cartDao.delete(cartEntity.getCartId());
        return new ResultBean(captureMoMoResponse.getPayUrl(), Constants.STATUS_201, "MOMO Payment successfully!!!");
    }

    @Override
    public ResultBean update(String json) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultBean delete(String json) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    private void getOrderDetailEntity(JsonObject json, OrderDetailEntity entity) throws Exception {
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.ORDER_DETAIL_ID)) {
            entity.setOrderDetailId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.ORDER_DETAIL_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.ORDER_ID)) {
            entity.setOrderId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.ORDER_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_ID)) {
            entity.setProductId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_ORDER_QUANTITY)) {
            entity.setProductOrderQuantity(
                    DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_ORDER_QUANTITY));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_ORDER_PRICE)) {
            entity.setProductOrderPrice(
                    DataUtil.getBigDecimalValueFromJsonByKey(json, ConstantColumns.PRODUCT_ORDER_PRICE));
        }
    }

    private void getOrderEntity(JsonObject json, OrderEntity entity) throws Exception {
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.ORDER_ID)) {
            entity.setOrderId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.ORDER_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.USER_ID)) {
            entity.setUserId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.USER_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.TOTAL)) {
            entity.setTotal(DataUtil.getBigDecimalValueFromJsonByKey(json, ConstantColumns.TOTAL));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PAYMENT)) {
            entity.setPayment(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PAYMENT));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.STATUS)) {
            entity.setStatus(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.STATUS));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.RECEIVER_NAME)) {
            entity.setReceiverName(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.RECEIVER_NAME));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.RECEIVER_PHONE)) {
            entity.setReceiverPhone(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.RECEIVER_PHONE));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.RECEIVER_ADDRESS)) {
            entity.setReceiverAddress(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.RECEIVER_ADDRESS));
        }
    }

    @Override
    public ResultBean getOrderById(Integer orderId) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResultBean getAllOrder() throws Exception {
        List<OrderReponse> listOrder = orderDao.getListOrder();
        return new ResultBean(listOrder, Constants.STATUS_OK, "Get all order successfully!!!");
    }

    @Override
    public ResultBean cancelOrder(Integer orderId, HttpServletRequest request) throws Exception {
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
        UserEntity userEntity = userDao.getUserByUsername(username);

        OrderEntity orderEntity = orderDao.getOrderById(orderId);

        if (orderEntity.getUserId() == userEntity.getUserId()) {
            orderEntity.setStatus("Đơn đã hủy");
            orderDao.update(orderEntity);
        }

        return new ResultBean(orderEntity, Constants.STATUS_OK, "Cancel order successfully!!!");
    }

    @Override
    public ResultBean getAllOfCustomer(HttpServletRequest request) throws Exception {
        final String requestTokenHeader = request.getHeader("Authorization");
        String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
        UserEntity userEntity = userDao.getUserByUsername(username);
        List<OrderReponse> listOrder = orderDao.getListOrderOfCustomer(userEntity.getUserId());
        return new ResultBean(listOrder, Constants.STATUS_OK, "Get all order successfully!!!");
    }

    @Override
    public ResultBean checkout(Integer orderId) throws Exception {
        OrderEntity orderEntity = orderDao.getOrderByIdMoMoCheck(orderId);
        orderEntity.setDelFlg("0");
        this.orderDao.update(orderEntity);
        return new ResultBean(orderEntity, Constants.STATUS_201, "Check out successfully!!!");
    }

}
