
package com.duongminhtai.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.bean.jpa.ProductEntity;
import com.duongminhtai.bean.reponse.ProductReponse;
import com.duongminhtai.dao.ProductDao;
import com.duongminhtai.service.ProductService;
import com.duongminhtai.utils.ApiValidateException;
import com.duongminhtai.utils.ConstantColumns;
import com.duongminhtai.utils.Constants;
import com.duongminhtai.utils.DataUtil;
import com.google.gson.JsonObject;

@Service
@Transactional(rollbackOn = { ApiValidateException.class, Exception.class })
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public ResultBean add(String json) throws Exception {
        ProductEntity entity = new ProductEntity();
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        this.getProductEntity(jsonObject, entity);
        productDao.add(entity);
        return new ResultBean(entity, Constants.STATUS_201, "Add successfully!!!");
    }

    @Override
    public ResultBean update(String json) throws Exception {
        ProductEntity entity = new ProductEntity();
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        this.getProductEntity(jsonObject, entity);
        productDao.update(entity);
        return new ResultBean(entity, Constants.STATUS_201, "Update successfully!!!");
    }

    @Override
    public ResultBean delete(String json) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    private void getProductEntity(JsonObject json, ProductEntity entity) throws Exception {
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_ID)) {
            entity.setProductId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.CATEGORY_ID)) {
            entity.setCategoryId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.CATEGORY_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_NAME)) {
            entity.setProductName(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PRODUCT_NAME));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_PRICE)) {
            entity.setProductPrice(DataUtil.getBigDecimalValueFromJsonByKey(json, ConstantColumns.PRODUCT_PRICE));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_QUANTITY)) {
            entity.setProductQuantity(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_QUANTITY));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_DESCRIPTION)) {
            entity.setProductDescription(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PRODUCT_DESCRIPTION));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_DETAIL)) {
            entity.setProductDetail(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PRODUCT_DETAIL));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_IMAGE)) {
            entity.setProductImage(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PRODUCT_IMAGE));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_ORIGIN)) {
            entity.setProductOrigin(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PRODUCT_ORIGIN));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_WEIGHT)) {
            entity.setProductWeight(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PRODUCT_WEIGHT));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_CARBOHYDRATE)) {
            entity.setProductCarbohydrate(DataUtil.getFloatValueFromJsonByKey(json, ConstantColumns.PRODUCT_CARBOHYDRATE));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_PROTEIN)) {
            entity.setProductProtein(DataUtil.getFloatValueFromJsonByKey(json, ConstantColumns.PRODUCT_PROTEIN));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_FAT)) {
            entity.setProductFat(DataUtil.getFloatValueFromJsonByKey(json, ConstantColumns.PRODUCT_FAT));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_CALORIES)) {
            entity.setProductCalories(DataUtil.getFloatValueFromJsonByKey(json, ConstantColumns.PRODUCT_CALORIES));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_SERVING_SIZE)) {
            entity.setProductServingSize(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_SERVING_SIZE));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_SERVINGS_PER_CONTAINER)) {
            entity.setProductServingsPerContainer(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_SERVINGS_PER_CONTAINER));
        }
    }

    @Override
    public ResultBean getProductById(Integer productId) throws Exception {
        ProductEntity productEntity = productDao.getProductById(productId);
        return new ResultBean(productEntity, Constants.STATUS_OK, "Get product by id successfully!!!");
    }

    @Override
    public ResultBean getListProductByCategoryId(Integer categoryId) throws Exception {
        List<ProductReponse> listProductReponse = productDao.getListProductByCategoryId(categoryId);
        return new ResultBean(listProductReponse, Constants.STATUS_OK, "Get list product category id successfully!!!");
    }

    @Override
    public ResultBean getAllProduct() throws Exception {
        List<ProductEntity> listProductEntity = productDao.getAllProduct();
        return new ResultBean(listProductEntity, Constants.STATUS_OK, "Get all product successfully!!!");
    }

    @Override
    public ResultBean searchProduct(String param, Integer categoryId, String weight, String origin) throws Exception {
        List<ProductReponse> listProductReponse = productDao.searchProduct(param, categoryId, weight, origin);
        return new ResultBean(listProductReponse, Constants.STATUS_OK, "Get all product successfully!!!");
    }

}
