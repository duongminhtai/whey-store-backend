
package com.duongminhtai.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.bean.jpa.UserEntity;
import com.duongminhtai.dao.UserDao;
import com.duongminhtai.service.UserService;
import com.duongminhtai.utils.ApiValidateException;
import com.duongminhtai.utils.ConstantColumns;
import com.duongminhtai.utils.Constants;
import com.duongminhtai.utils.DataUtil;
import com.google.gson.JsonObject;

@Service
@Transactional(rollbackOn = { ApiValidateException.class, Exception.class })
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public ResultBean add(String json) throws Exception {
        UserEntity entity = new UserEntity();
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        this.getUserEntity(jsonObject, entity);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userDao.add(entity);
        return new ResultBean(entity, Constants.STATUS_201, "Add successfully!!!");
    }

    @Override
    public ResultBean update(String json) throws Exception {
        UserEntity entity = new UserEntity();
        JsonObject jsonObject = DataUtil.getJsonObject(json);
        this.getUserEntity(jsonObject, entity);
        userDao.update(entity);
        return new ResultBean(entity, Constants.STATUS_201, "Update successfully!!!");
    }

    @Override
    public ResultBean delete(String json) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    private void getUserEntity(JsonObject json, UserEntity entity) throws Exception {
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.USER_ID)) {
            entity.setUserId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.USER_ID));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.USERNAME)) {
            entity.setUsername(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.USERNAME));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PASSWORD)) {
            entity.setPassword(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.PASSWORD));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.FULLNAME)) {
            entity.setFullname(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.FULLNAME));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.ROLE)) {
            entity.setRole(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.ROLE));
        }
        if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.ACTIVE)) {
            entity.setActive(DataUtil.getStringValueFromJsonByKey(json, ConstantColumns.ACTIVE));
        }
    }

    @Override
    public ResultBean getUserById(Integer userId) throws Exception {
        UserEntity entity = userDao.getUserById(userId);
        return new ResultBean(entity, Constants.STATUS_OK, "Get user by id successfully!!!");
    }

    @Override
    public ResultBean getAllUser() throws Exception {
        List<UserEntity> listUserEntity = userDao.getAllUser();
        return new ResultBean(listUserEntity, Constants.STATUS_OK, "Get all user successfully!!!");
    }

}
