package com.duongminhtai.service.impl;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.duongminhtai.bean.ResultBean;
import com.duongminhtai.bean.jpa.CartDetailEntity;
import com.duongminhtai.bean.jpa.CartEntity;
import com.duongminhtai.bean.jpa.ProductEntity;
import com.duongminhtai.bean.jpa.UserEntity;
import com.duongminhtai.bean.reponse.CartDetailReponse;
import com.duongminhtai.bean.reponse.CartReponse;
import com.duongminhtai.config.JwtTokenUtil;
import com.duongminhtai.dao.CartDao;
import com.duongminhtai.dao.CartDetailDao;
import com.duongminhtai.dao.ProductDao;
import com.duongminhtai.dao.UserDao;
import com.duongminhtai.service.CartService;
import com.duongminhtai.utils.ApiValidateException;
import com.duongminhtai.utils.ConstantColumns;
import com.duongminhtai.utils.Constants;
import com.duongminhtai.utils.DataUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service
@Transactional(rollbackOn = { ApiValidateException.class, Exception.class })
public class CartServiceImpl implements CartService {

	@Autowired
	private JwtTokenUtil jwtUtil;

	@Autowired
	private CartDao cartDao;

	@Autowired
	private CartDetailDao cartDetailDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private UserDao userDao;

	@Override
	public ResultBean add(String json, HttpServletRequest request) throws Exception {
		// get user by token
		final String requestTokenHeader = request.getHeader("Authorization");
		String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
		UserEntity userEntity = userDao.getUserByUsername(username);

		// get request
		JsonObject jsonObject = DataUtil.getJsonObject(json);
		CartEntity cartEntity = new CartEntity();
		this.getCartEntity(jsonObject, cartEntity);
		CartDetailEntity cartDetailEntity = new CartDetailEntity();
		this.getCartDetailEntity(jsonObject, cartDetailEntity);

		// add cart, add cart detail
		if (cartEntity.getCartId() == null && cartDetailEntity.getCartDetailId() == null) {
			ProductEntity productEntity = productDao.getProductById(cartDetailEntity.getProductId());
			// add cart
			cartEntity.setTotal(productEntity.getProductPrice());
			cartEntity.setUserId(userEntity.getUserId());
			cartDao.add(cartEntity);
			Integer cartId = cartDao.getMaxIdCart();
			// add cart detail
			cartDetailEntity.setProductCartPrice(productEntity.getProductPrice());
			cartDetailEntity.setProductCartQuantity(1);
			cartDetailEntity.setProductId(productEntity.getProductId());
			cartDetailEntity.setCartId(cartId);
			cartDetailDao.add(cartDetailEntity);
			return new ResultBean(cartEntity, Constants.STATUS_201, "Add successfully!!!");
		}

		// update cart, add cart detail
		if (cartEntity.getCartId() != null && cartDetailEntity.getCartDetailId() == null) {
			ProductEntity productEntity = productDao.getProductById(cartDetailEntity.getProductId());
			// update cart
			cartEntity = cartDao.getCartById(cartEntity.getCartId());
			cartEntity.setTotal(cartEntity.getTotal().add(productEntity.getProductPrice()));
			cartDao.update(cartEntity);
			// add cart detail
			cartDetailEntity.setProductCartPrice(productEntity.getProductPrice());
			cartDetailEntity.setProductCartQuantity(1);
			cartDetailEntity.setProductId(productEntity.getProductId());
			cartDetailDao.add(cartDetailEntity);
			return new ResultBean(cartEntity, Constants.STATUS_201, "Add successfully!!!");
		}

		// update cart, add cart detail
		if (cartEntity.getCartId() != null && cartDetailEntity.getCartDetailId() != null) {
			ProductEntity productEntity = productDao.getProductById(cartDetailEntity.getProductId());
			// update cart
			cartEntity = cartDao.getCartById(cartEntity.getCartId());
			cartEntity.setTotal(cartEntity.getTotal().add(productEntity.getProductPrice()));
			cartDao.update(cartEntity);
			// update cart detail
			cartDetailEntity = cartDetailDao.getCartDetailEntityById(cartDetailEntity.getCartDetailId());
			cartDetailEntity
					.setProductCartPrice(cartDetailEntity.getProductCartPrice().add(productEntity.getProductPrice()));
			cartDetailEntity.setProductCartQuantity(cartDetailEntity.getProductCartQuantity() + 1);
			cartDetailDao.update(cartDetailEntity);
			return new ResultBean(cartEntity, Constants.STATUS_201, "Add successfully!!!");
		}
		return new ResultBean(cartEntity, Constants.STATUS_201, "Add successfully!!!");
	}

	@Override
	public ResultBean addMultil(String json, HttpServletRequest request) throws Exception {
		// get user by token
		final String requestTokenHeader = request.getHeader("Authorization");
		String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
		UserEntity userEntity = userDao.getUserByUsername(username);

		JsonObject jsonObject = DataUtil.getJsonObject(json);
		CartEntity cartEntity = new CartEntity();
		this.getCartEntity(jsonObject, cartEntity);
		cartEntity.setUserId(userEntity.getUserId());

		int checkCart = cartDao.checkCartExist(userEntity.getUserId());
		if (checkCart <= 0) {
			// add cart
			cartDao.add(cartEntity);

			// add cart detail
			Integer cartId = cartDao.getMaxIdCart();
			JsonArray jsonArray = DataUtil.getJsonArrayByKey(jsonObject, ConstantColumns.CART_DETAIL);
			for (JsonElement jsonElement : jsonArray) {
				CartDetailEntity cartDetailEntity = new CartDetailEntity();
				this.getCartDetailEntity((JsonObject) jsonElement, cartDetailEntity);
				cartDetailEntity.setCartId(cartId);
				cartDetailDao.add(cartDetailEntity);
			}
			return new ResultBean(cartEntity, Constants.STATUS_201, "Add multil successfully!!!");
		} else {
			// delete cart exist
			cartDao.delete(cartDao.getCartByUserId(userEntity.getUserId()).getCartId());

			// add cart
			cartDao.add(cartEntity);

			// add cart detail
			Integer cartId = cartDao.getMaxIdCart();
			JsonArray jsonArray = DataUtil.getJsonArrayByKey(jsonObject, ConstantColumns.CART_DETAIL);
			for (JsonElement jsonElement : jsonArray) {
				CartDetailEntity cartDetailEntity = new CartDetailEntity();
				this.getCartDetailEntity((JsonObject) jsonElement, cartDetailEntity);
				cartDetailEntity.setCartId(cartId);
				cartDetailDao.add(cartDetailEntity);
			}
			return new ResultBean(cartEntity, Constants.STATUS_201, "Add multil successfully!!!");
		}
	}

	@Override
	public ResultBean update(String json, HttpServletRequest request) throws Exception {
		JsonObject jsonObject = DataUtil.getJsonObject(json);
		CartEntity cartEntity = new CartEntity();
		this.getCartEntity(jsonObject, cartEntity);

		cartDao.update(cartEntity);
		JsonArray jsonArray = DataUtil.getJsonArrayByKey(jsonObject, ConstantColumns.CART_DETAIL);
		for (JsonElement jsonElement : jsonArray) {
			CartDetailEntity cartDetailEntity = new CartDetailEntity();
			this.getCartDetailEntity((JsonObject) jsonElement, cartDetailEntity);
			cartDetailEntity.setCartId(cartEntity.getCartId());
			if (cartDetailEntity.getProductCartQuantity() == 0) {
				cartDetailEntity.setDelFlg("1");
			}
			cartDetailDao.update(cartDetailEntity);
		}
		return new ResultBean(cartEntity, Constants.STATUS_201, "Update successfully!!!");
	}

	@SuppressWarnings("unused")
	private void getCartDetailEntity(JsonObject json, CartDetailEntity entity) throws Exception {
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.CART_DETAIL_ID)) {
			entity.setCartDetailId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.CART_DETAIL_ID));
		}
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.CART_ID)) {
			entity.setCartId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.CART_ID));
		}
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_ID)) {
			entity.setProductId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_ID));
		}
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_CART_QUANTITY)) {
			entity.setProductCartQuantity(
					DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.PRODUCT_CART_QUANTITY));
		}
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.PRODUCT_CART_PRICE)) {
			entity.setProductCartPrice(
					DataUtil.getBigDecimalValueFromJsonByKey(json, ConstantColumns.PRODUCT_CART_PRICE));
		}
	}

	@SuppressWarnings("unused")
	private void getCartEntity(JsonObject json, CartEntity entity) throws Exception {
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.CART_ID)) {
			entity.setCartId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.CART_ID));
		}
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.USER_ID)) {
			entity.setUserId(DataUtil.getIntValueFromJsonByKey(json, ConstantColumns.USER_ID));
		}
		if (DataUtil.isJsonObjectHasKey(json, ConstantColumns.TOTAL)) {
			entity.setTotal(DataUtil.getBigDecimalValueFromJsonByKey(json, ConstantColumns.TOTAL));
		}
	}

	@Override
	public ResultBean getCart(HttpServletRequest request) throws Exception {
		final String requestTokenHeader = request.getHeader("Authorization");
		String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
		UserEntity userEntity = userDao.getUserByUsername(username);

		CartEntity cartEntity = cartDao.getCartByUserId(userEntity.getUserId());
		List<CartDetailReponse> listReponse = cartDetailDao.getListCartDetailByCartId(cartEntity.getCartId());

		BigDecimal total = new BigDecimal(0);
		for (CartDetailReponse cartDetailReponse : listReponse) {
			total = total.add(cartDetailReponse.getProductCartPrice());
		}
		cartEntity.setTotal(total);
		cartDao.update(cartEntity);

		CartReponse cartReponse = new CartReponse();
		cartReponse.setCartId(cartEntity.getCartId());
		cartReponse.setUserId(cartEntity.getUserId());
		cartReponse.setTotal(total);
		cartReponse.setCartDetail(listReponse);
		return new ResultBean(cartReponse, Constants.STATUS_OK, "Get cart by user id successfully!!!");
	}

	@Override
	public ResultBean delete(HttpServletRequest request) throws Exception {
		final String requestTokenHeader = request.getHeader("Authorization");
		String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
		UserEntity userEntity = userDao.getUserByUsername(username);
		CartEntity cartEntity = cartDao.getCartByUserId(userEntity.getUserId());
		boolean check = cartDao.delete(cartEntity.getCartId());
		return new ResultBean(check, Constants.STATUS_OK, "Delete cart successfully!!!");
	}

	@Override
	public ResultBean deleteCartDetail(Integer cartDetailId, HttpServletRequest request) throws Exception {
		final String requestTokenHeader = request.getHeader("Authorization");
		String username = jwtUtil.getUsernameFromToken(requestTokenHeader.substring(7));
		UserEntity userEntity = userDao.getUserByUsername(username);
		CartEntity cartEntity = cartDao.getCartByUserId(userEntity.getUserId());
		CartDetailEntity cartDetailEntity = cartDetailDao.getCartDetailEntityById(cartDetailId);
		cartEntity.setTotal(cartEntity.getTotal().subtract(cartDetailEntity.getProductCartPrice()));
		this.cartDao.update(cartEntity);
		this.cartDetailDao.delete(cartDetailId);
		return new ResultBean(cartEntity, Constants.STATUS_OK, "Delete cart detail successfully!!!");
	}

}
