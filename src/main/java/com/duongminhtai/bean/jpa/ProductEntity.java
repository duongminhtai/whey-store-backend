package com.duongminhtai.bean.jpa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "t_product")
@NamedQuery(name = "ProductEntity.findAll", query = "SELECT p FROM ProductEntity p")
public class ProductEntity extends CommonEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_id")
    @JsonProperty("product_id")
    private Integer productId;

    @Column(name = "category_id")
    @JsonProperty("category_id")
    private Integer categoryId;

    @Column(name = "product_name")
    @JsonProperty("product_name")
    private String productName;

    @Column(name = "product_price")
    @JsonProperty("product_price")
    private BigDecimal productPrice;

    @Column(name = "product_quantity")
    @JsonProperty("product_quantity")
    private Integer productQuantity;

    @Column(name = "product_description", columnDefinition = "text")
    @JsonProperty("product_description")
    private String productDescription;

    @Column(name = "product_detail", columnDefinition = "text")
    @JsonProperty("product_detail")
    private String productDetail;

    @Column(name = "product_image", columnDefinition = "text")
    @JsonProperty("product_image")
    private String productImage;

    @Column(name = "product_origin")
    @JsonProperty("product_origin")
    private String productOrigin;

    @Column(name = "product_weight")
    @JsonProperty("product_weight")
    private String productWeight;

    @Column(name = "product_carbohydrate")
    @JsonProperty("product_carbohydrate")
    private Float productCarbohydrate;

    @Column(name = "product_protein")
    @JsonProperty("product_protein")
    private Float productProtein;

    @Column(name = "product_fat")
    @JsonProperty("product_fat")
    private Float productFat;

    @Column(name = "product_calories")
    @JsonProperty("product_calories")
    private Float productCalories;

    @Column(name = "product_serving_size")
    @JsonProperty("product_serving_size")
    private Integer productServingSize;

    @Column(name = "product_servings_per_container")
    @JsonProperty("product_servings_per_container")
    private Integer productServingsPerContainer;

    /**
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * @return the categoryId
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the productPrice
     */
    public BigDecimal getProductPrice() {
        return productPrice;
    }

    /**
     * @param productPrice the productPrice to set
     */
    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * @return the productQuantity
     */
    public Integer getProductQuantity() {
        return productQuantity;
    }

    /**
     * @param productQuantity the productQuantity to set
     */
    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    /**
     * @return the productDescription
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * @param productDescription the productDescription to set
     */
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    /**
     * @return the productDetail
     */
    public String getProductDetail() {
        return productDetail;
    }

    /**
     * @param productDetail the productDetail to set
     */
    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    /**
     * @return the productImage
     */
    public String getProductImage() {
        return productImage;
    }

    /**
     * @param productImage the productImage to set
     */
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    /**
     * @return the productOrigin
     */
    public String getProductOrigin() {
        return productOrigin;
    }

    /**
     * @param productOrigin the productOrigin to set
     */
    public void setProductOrigin(String productOrigin) {
        this.productOrigin = productOrigin;
    }

    /**
     * @return the productWeight
     */
    public String getProductWeight() {
        return productWeight;
    }

    /**
     * @param productWeight the productWeight to set
     */
    public void setProductWeight(String productWeight) {
        this.productWeight = productWeight;
    }

    /**
     * @return the productCarbohydrate
     */
    public Float getProductCarbohydrate() {
        return productCarbohydrate;
    }

    /**
     * @param productCarbohydrate the productCarbohydrate to set
     */
    public void setProductCarbohydrate(Float productCarbohydrate) {
        this.productCarbohydrate = productCarbohydrate;
    }

    /**
     * @return the productProtein
     */
    public Float getProductProtein() {
        return productProtein;
    }

    /**
     * @param productProtein the productProtein to set
     */
    public void setProductProtein(Float productProtein) {
        this.productProtein = productProtein;
    }

    /**
     * @return the productFat
     */
    public Float getProductFat() {
        return productFat;
    }

    /**
     * @param productFat the productFat to set
     */
    public void setProductFat(Float productFat) {
        this.productFat = productFat;
    }

    /**
     * @return the productCalories
     */
    public Float getProductCalories() {
        return productCalories;
    }

    /**
     * @param productCalories the productCalories to set
     */
    public void setProductCalories(Float productCalories) {
        this.productCalories = productCalories;
    }

    /**
     * @return the productServingSize
     */
    public Integer getProductServingSize() {
        return productServingSize;
    }

    /**
     * @param productServingSize the productServingSize to set
     */
    public void setProductServingSize(Integer productServingSize) {
        this.productServingSize = productServingSize;
    }

    /**
     * @return the productServingsPerContainer
     */
    public Integer getProductServingsPerContainer() {
        return productServingsPerContainer;
    }

    /**
     * @param productServingsPerContainer the productServingsPerContainer to set
     */
    public void setProductServingsPerContainer(Integer productServingsPerContainer) {
        this.productServingsPerContainer = productServingsPerContainer;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
