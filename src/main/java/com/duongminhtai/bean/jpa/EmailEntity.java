package com.duongminhtai.bean.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "t_email")
@NamedQuery(name = "EmailEntity.findAll", query = "SELECT cd FROM EmailEntity cd")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EmailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @JsonProperty("id")
    private Integer id;

    @Column(name = "email_from")
    @JsonProperty("email_from")
    private String emailFrom;

    @Column(name = "email_to")
    @JsonProperty("email_to")
    private String emailTo;

    @Column(name = "email_title")
    @JsonProperty("email_title")
    private String emailTitle;

    @Column(name = "email_content")
    @JsonProperty("email_content")
    private String emailContent;
}
