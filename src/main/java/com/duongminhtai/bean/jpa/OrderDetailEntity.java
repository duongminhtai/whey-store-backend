package com.duongminhtai.bean.jpa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
//@Table(name = "t_order_detail", schema = "wheystore")
@Table(name = "t_order_detail")
@NamedQuery(name = "OrderDetailEntity.findAll", query = "SELECT od FROM OrderDetailEntity od")
public class OrderDetailEntity extends CommonEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "order_detail_id")
	@JsonProperty("order_detail_id")
	private Integer orderDetailId;

	@Column(name = "order_id")
	@JsonProperty("order_id")
	private Integer orderId;

	@Column(name = "product_id")
	@JsonProperty("product_id")
	private Integer productId;

	@Column(name = "product_order_quantity")
	@JsonProperty("product_order_quantity")
	private Integer productOrderQuantity;

	@Column(name = "product_order_price")
	@JsonProperty("product_order_price")
	private BigDecimal productOrderPrice;

	/**
	 * @return the orderDetailId
	 */
	public Integer getOrderDetailId() {
		return orderDetailId;
	}

	/**
	 * @param orderDetailId the orderDetailId to set
	 */
	public void setOrderDetailId(Integer orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	/**
	 * @return the orderId
	 */
	public Integer getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @return the productOrderQuantity
	 */
	public Integer getProductOrderQuantity() {
		return productOrderQuantity;
	}

	/**
	 * @param productOrderQuantity the productOrderQuantity to set
	 */
	public void setProductOrderQuantity(Integer productOrderQuantity) {
		this.productOrderQuantity = productOrderQuantity;
	}

	/**
	 * @return the productOrderPrice
	 */
	public BigDecimal getProductOrderPrice() {
		return productOrderPrice;
	}

	/**
	 * @param productOrderPrice the productOrderPrice to set
	 */
	public void setProductOrderPrice(BigDecimal productOrderPrice) {
		this.productOrderPrice = productOrderPrice;
	}

}
