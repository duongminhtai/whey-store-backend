package com.duongminhtai.bean.jpa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
//@Table(name = "t_cart_detail", schema = "wheystore")
@Table(name = "t_cart_detail")
@NamedQuery(name = "CartDetailEntity.findAll", query = "SELECT cd FROM CartDetailEntity cd")
public class CartDetailEntity extends CommonEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cart_detail_id")
	@JsonProperty("cart_detail_id")
	private Integer cartDetailId;

	@Column(name = "cart_id")
	@JsonProperty("cart_id")
	private Integer cartId;

	@Column(name = "product_id")
	@JsonProperty("product_id")
	private Integer productId;

	@Column(name = "product_cart_quantity")
	@JsonProperty("product_cart_quantity")
	private Integer productCartQuantity;

	@Column(name = "product_cart_price")
	@JsonProperty("product_cart_price")
	private BigDecimal productCartPrice;

	/**
	 * @return the cartDetailId
	 */
	public Integer getCartDetailId() {
		return cartDetailId;
	}

	/**
	 * @param cartDetailId the cartDetailId to set
	 */
	public void setCartDetailId(Integer cartDetailId) {
		this.cartDetailId = cartDetailId;
	}

	/**
	 * @return the cartId
	 */
	public Integer getCartId() {
		return cartId;
	}

	/**
	 * @param cartId the cartId to set
	 */
	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}

	/**
	 * @return the productId
	 */
	public Integer getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	/**
	 * @return the productCartQuantity
	 */
	public Integer getProductCartQuantity() {
		return productCartQuantity;
	}

	/**
	 * @param productCartQuantity the productCartQuantity to set
	 */
	public void setProductCartQuantity(Integer productCartQuantity) {
		this.productCartQuantity = productCartQuantity;
	}

	/**
	 * @return the productCartPrice
	 */
	public BigDecimal getProductCartPrice() {
		return productCartPrice;
	}

	/**
	 * @param productCartPrice the productCartPrice to set
	 */
	public void setProductCartPrice(BigDecimal productCartPrice) {
		this.productCartPrice = productCartPrice;
	}

}
