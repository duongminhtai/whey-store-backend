package com.duongminhtai.bean.reponse;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartDetailReponse extends CommonResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("cart_detail_id")
    private Integer cartDetailId;

    @JsonProperty("product_id")
    private Integer productId;

    @JsonProperty("product_price")
    private BigDecimal productPrice;

    @JsonProperty("product_name")
    private String productName;

    @JsonProperty("product_image")
    private String productImage;

    @JsonProperty("product_cart_quantity")
    private Integer productCartQuantity;

    @JsonProperty("product_cart_price")
    private BigDecimal productCartPrice;

    public CartDetailReponse(Integer cartDetailId, Integer productId, BigDecimal productPrice, String productName, String productImage,
            Integer productCartQuantity, BigDecimal productCartPrice, String delFlg) {
        super();
        this.cartDetailId = cartDetailId;
        this.productId = productId;
        this.productPrice = productPrice;
        this.productName = productName;
        this.productImage = productImage;
        this.productCartQuantity = productCartQuantity;
        this.productCartPrice = productCartPrice;
        setDelFlg(delFlg);
    }

    public Integer getCartDetailId() {
        return cartDetailId;
    }

    public void setCartDetailId(Integer cartDetailId) {
        this.cartDetailId = cartDetailId;
    }

    /**
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the productImage
     */
    public String getProductImage() {
        return productImage;
    }

    /**
     * @param productImage the productImage to set
     */
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    /**
     * @return the productCartQuantity
     */
    public Integer getProductCartQuantity() {
        return productCartQuantity;
    }

    /**
     * @param productCartQuantity the productCartQuantity to set
     */
    public void setProductCartQuantity(Integer productCartQuantity) {
        this.productCartQuantity = productCartQuantity;
    }

    /**
     * @return the productCartPrice
     */
    public BigDecimal getProductCartPrice() {
        return productCartPrice;
    }

    /**
     * @param productCartPrice the productCartPrice to set
     */
    public void setProductCartPrice(BigDecimal productCartPrice) {
        this.productCartPrice = productCartPrice;
    }

    public BigDecimal getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

}
