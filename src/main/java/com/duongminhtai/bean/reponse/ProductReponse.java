package com.duongminhtai.bean.reponse;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductReponse extends CommonResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("product_id")
    private Integer productId;

    @JsonProperty("category_id")
    private Integer categoryId;
    
    @JsonProperty("category_name")
    private String categoryName;

    @JsonProperty("product_name")
    private String productName;

    @JsonProperty("product_price")
    private BigDecimal productPrice;

    @JsonProperty("product_quantity")
    private Integer productQuantity;

    @JsonProperty("product_description")
    private String productDescription;

    @JsonProperty("product_detail")
    private String productDetail;

    @JsonProperty("product_image")
    private String productImage;

    @JsonProperty("product_origin")
    private String productOrigin;

    @JsonProperty("product_weight")
    private String productWeight;

    @JsonProperty("product_carbohydrate")
    private Float productCarbohydrate;

    @JsonProperty("product_protein")
    private Float productProtein;

    @JsonProperty("product_fat")
    private Float productFat;

    @JsonProperty("product_calories")
    private Float productCalories;

    @JsonProperty("product_serving_size")
    private Integer productServingSize;

    @JsonProperty("product_servings_per_container")
    private Integer productServingsPerContainer;

    /**
     * @return the productId
     */
    public Integer getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    /**
     * @return the categoryId
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * @param categoryId the categoryId to set
     */
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * @return the productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * @param productName the productName to set
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * @return the productPrice
     */
    public BigDecimal getProductPrice() {
        return productPrice;
    }

    /**
     * @param productPrice the productPrice to set
     */
    public void setProductPrice(BigDecimal productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * @return the productQuantity
     */
    public Integer getProductQuantity() {
        return productQuantity;
    }

    /**
     * @param productQuantity the productQuantity to set
     */
    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    /**
     * @return the productDescription
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * @param productDescription the productDescription to set
     */
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    /**
     * @return the productDetail
     */
    public String getProductDetail() {
        return productDetail;
    }

    /**
     * @param productDetail the productDetail to set
     */
    public void setProductDetail(String productDetail) {
        this.productDetail = productDetail;
    }

    /**
     * @return the productImage
     */
    public String getProductImage() {
        return productImage;
    }

    /**
     * @param productImage the productImage to set
     */
    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    /**
     * @return the productOrigin
     */
    public String getProductOrigin() {
        return productOrigin;
    }

    /**
     * @param productOrigin the productOrigin to set
     */
    public void setProductOrigin(String productOrigin) {
        this.productOrigin = productOrigin;
    }

    /**
     * @return the productWeight
     */
    public String getProductWeight() {
        return productWeight;
    }

    /**
     * @param productWeight the productWeight to set
     */
    public void setProductWeight(String productWeight) {
        this.productWeight = productWeight;
    }

    /**
     * @return the productCarbohydrate
     */
    public Float getProductCarbohydrate() {
        return productCarbohydrate;
    }

    /**
     * @param productCarbohydrate the productCarbohydrate to set
     */
    public void setProductCarbohydrate(Float productCarbohydrate) {
        this.productCarbohydrate = productCarbohydrate;
    }

    /**
     * @return the productProtein
     */
    public Float getProductProtein() {
        return productProtein;
    }

    /**
     * @param productProtein the productProtein to set
     */
    public void setProductProtein(Float productProtein) {
        this.productProtein = productProtein;
    }

    /**
     * @return the productFat
     */
    public Float getProductFat() {
        return productFat;
    }

    /**
     * @param productFat the productFat to set
     */
    public void setProductFat(Float productFat) {
        this.productFat = productFat;
    }

    /**
     * @return the productCalories
     */
    public Float getProductCalories() {
        return productCalories;
    }

    /**
     * @param productCalories the productCalories to set
     */
    public void setProductCalories(Float productCalories) {
        this.productCalories = productCalories;
    }

    /**
     * @return the productServingSize
     */
    public Integer getProductServingSize() {
        return productServingSize;
    }

    /**
     * @param productServingSize the productServingSize to set
     */
    public void setProductServingSize(Integer productServingSize) {
        this.productServingSize = productServingSize;
    }

    /**
     * @return the productServingsPerContainer
     */
    public Integer getProductServingsPerContainer() {
        return productServingsPerContainer;
    }

    /**
     * @param productServingsPerContainer the productServingsPerContainer to set
     */
    public void setProductServingsPerContainer(Integer productServingsPerContainer) {
        this.productServingsPerContainer = productServingsPerContainer;
    }

    /**
     * @param productId
     * @param categoryId
     * @param productName
     * @param productPrice
     * @param productQuantity
     * @param productDescription
     * @param productDetail
     * @param productImage
     * @param productOrigin
     * @param productWeight
     * @param productCarbohydrate
     * @param productProtein
     * @param productFat
     * @param productCalories
     * @param productServingSize
     * @param productServingsPerContainer
     */
    public ProductReponse(Integer productId, Integer categoryId, String categoryName, String productName, BigDecimal productPrice, Integer productQuantity,
            String productDescription, String productDetail, String productImage, String productOrigin, String productWeight, Float productCarbohydrate,
            Float productProtein, Float productFat, Float productCalories, Integer productServingSize, Integer productServingsPerContainer, String delFlg) {
        super();
        this.productId = productId;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productQuantity = productQuantity;
        this.productDescription = productDescription;
        this.productDetail = productDetail;
        this.productImage = productImage;
        this.productOrigin = productOrigin;
        this.productWeight = productWeight;
        this.productCarbohydrate = productCarbohydrate;
        this.productProtein = productProtein;
        this.productFat = productFat;
        this.productCalories = productCalories;
        this.productServingSize = productServingSize;
        this.productServingsPerContainer = productServingsPerContainer;
        setDelFlg(delFlg);
    }

}
