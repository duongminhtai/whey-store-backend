
package com.duongminhtai.bean.reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CommonResponse {

    @JsonProperty("del_flg")
    private String delFlg = "0";

    @JsonProperty("create_by")
    private Integer createBy;

    public String getDelFlg() {
        return delFlg;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }
}
