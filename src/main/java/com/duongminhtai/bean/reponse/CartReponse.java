package com.duongminhtai.bean.reponse;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CartReponse extends CommonResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("cart_id")
    private Integer cartId;

    @JsonProperty("user_id")
    private Integer userId;

    @JsonProperty("total")
    private BigDecimal total;

    @JsonProperty("cart_detail")
    private List<CartDetailReponse> cartDetail;

    /**
     * @return the cartId
     */
    public Integer getCartId() {
        return cartId;
    }

    /**
     * @param cartId the cartId to set
     */
    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    /**
     * @return the cartDetail
     */
    public List<CartDetailReponse> getCartDetail() {
        return cartDetail;
    }

    /**
     * @param cartDetail the cartDetail to set
     */
    public void setCartDetail(List<CartDetailReponse> cartDetail) {
        this.cartDetail = cartDetail;
    }

}
