package com.duongminhtai.builderpattern;

public interface Mint {
    void mintCoin();

    default void gokuCoin() {
        
    };
}
