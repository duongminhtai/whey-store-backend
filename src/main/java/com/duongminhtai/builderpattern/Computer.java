package com.duongminhtai.builderpattern;

/**
 * Builder Pattern is a design used to build object with a large amount of class
 * and parameters. It does not seem to be useful in small class which does not
 * have many constructors.
 * 
 * @author BAP
 *
 */
public class Computer {

    public static class Builder {
        private String cPU;
        private String ram;
        private String rom;
        private String mainboard;
        private String harddisk;
        private String keyboard;
        private String mouse;
        private String screen;

        public Builder withCPU(String cPU) {
            this.cPU = cPU;
            return this;
        }

        public Builder withRAM(String ram) {
            this.ram = ram;
            return this;
        }

        public Builder withROM(String rom) {
            this.rom = rom;
            return this;
        }

        public Builder withMainboard(String mainboard) {
            this.mainboard = mainboard;
            return this;
        }

        public Builder withHarddisk(String harddisk) {
            this.harddisk = harddisk;
            return this;
        }

        public Builder withKeyboard(String keyboard) {
            this.keyboard = keyboard;
            return this;
        }

        public Builder withMouse(String mouse) {
            this.mouse = mouse;
            return this;
        }

        public Builder withScreen(String screen) {
            this.screen = screen;
            return this;
        }

        public Computer build() {
            Computer computer = new Computer();
            computer.cPU = this.cPU;
            computer.ram = this.ram;
            computer.rom = this.rom;
            computer.mainboard = this.mainboard;
            computer.harddisk = this.harddisk;
            computer.keyboard = this.keyboard;
            computer.mouse = this.mouse;
            computer.screen = this.screen;
            return computer;
        }
    }

    //
    private Computer() {
    }

    // Computer's fields
    private String cPU;
    private String ram;
    private String rom;
    private String mainboard;
    private String harddisk;
    private String keyboard;
    private String mouse;
    private String screen;

    public String getcPU() {
        return cPU;
    }

    public void setcPU(String cPU) {
        this.cPU = cPU;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getRom() {
        return rom;
    }

    public void setRom(String rom) {
        this.rom = rom;
    }

    public String getMainboard() {
        return mainboard;
    }

    public void setMainboard(String mainboard) {
        this.mainboard = mainboard;
    }

    public String getHarddisk() {
        return harddisk;
    }

    public void setHarddisk(String harddisk) {
        this.harddisk = harddisk;
    }

    public String getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

    public String getMouse() {
        return mouse;
    }

    public void setMouse(String mouse) {
        this.mouse = mouse;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    @Override
    public String toString() {
        return "Computer [cPU=" + cPU + ", ram=" + ram + ", rom=" + rom + ", mainboard=" + mainboard + ", harddisk="
                + harddisk + ", keyboard=" + keyboard + ", mouse=" + mouse + ", screen=" + screen + "]";
    }

}
